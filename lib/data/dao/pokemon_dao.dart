import '../extensions.dart';
import '../local_database.dart';
import '../models/pokemon.dart';

class PokemonDao {
  factory PokemonDao() => _instance;

  const PokemonDao._();

  static const _instance = PokemonDao._();

  Future<Pokemon?> getByName(String name) async {
    final box = await LocalDatabase().pokemonBox;
    final map = await box.get(name);
    return map == null
        ? null
        : Pokemon.fromMap(map.deepCast<String, Object?>());
  }

  Future<List<Pokemon>> getAll() async {
    final box = await LocalDatabase().pokemonBox;
    final values = (await box.getAllValues()).values;
    final items = values.map<Map<String, Object?>?>(
      (e) => e?.deepCast<String, Object?>(),
    );
    return items.map(Pokemon.fromMap).toList();
  }

  Future<void> save(Pokemon pokemon) async {
    final box = await LocalDatabase().pokemonBox;
    await box.put(pokemon.name, pokemon.toMap());
  }

  Future<void> saveAll(List<Pokemon> items) async {
    for (final item in items) {
      await save(item);
    }
  }
}
