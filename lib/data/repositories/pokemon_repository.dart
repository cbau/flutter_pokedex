import 'dart:convert';

import '../dao/pokemon_dao.dart';
import '../models/pokedex.dart';
import '../models/pokemon.dart';
import '../models/pokemon_loader.dart';
import '../services/poke_api_service.dart';

class PokemonRepository {
  factory PokemonRepository() => _instance;

  const PokemonRepository._();

  static const _instance = PokemonRepository._();

  Future<List<Pokemon>> get all async {
    var items = await PokemonDao().getAll();
    if (items.isNotEmpty) {
      return items;
    }

    final response = await PokeApiService().getPokedexById('2');

    if (response.statusCode < 400) {
      final map = jsonDecode(response.body) as Map<String, Object?>?;
      final pokedex = Pokedex.fromMap(map);
      items = pokedex.pokemonEntries
          .map(
            (e) => Pokemon(
              name: e.pokemonSpecies.name,
            ),
          )
          .toList();

      await PokemonDao().saveAll(items);

      return items;
    }

    throw Exception('${response.statusCode} ${response.reasonPhrase}');
  }

  Future<Pokemon> getById(int id) async => getByName(id.toString());

  Future<Pokemon> getByName(String name) async {
    var pokemon = await PokemonDao().getByName(name);
    if (pokemon != null && pokemon.id > 0) {
      return pokemon;
    }

    final response = await PokeApiService().getPokemonById(name);

    if (response.statusCode < 400) {
      final map = jsonDecode(response.body) as Map<String, Object?>?;
      pokemon = Pokemon.fromMap(map);

      await PokemonDao().save(pokemon);

      return pokemon;
    }

    throw Exception('${response.statusCode} ${response.reasonPhrase}');
  }

  Stream<PokemonLoader> loadData() async* {
    final items = await all;
    yield PokemonLoader(
      items: items,
    );

    var hasDataChanged = false;

    for (var i = 0; i < items.length; i++) {
      if (items[i].id <= 0) {
        items[i] = await getByName(items[i].name);
        yield PokemonLoader(
          items: items,
          progress: (i + 1) / items.length,
        );
        hasDataChanged = true;
      }
    }

    if (!hasDataChanged) {
      yield PokemonLoader(
        items: items,
        progress: 1,
      );
    }
  }
}
