extension MapExtension on Map<dynamic, dynamic> {
  Map<K, V> deepCast<K, V>() {
    final result = <K, V>{};
    for (final key in keys) {
      if (this[key] is Map) {
        result[key as K] = (this[key] as Map).deepCast<K, V>() as V;
      } else if (this[key] is List) {
        result[key as K] = (this[key] as List)
            .map((e) => e is Map ? e.deepCast<K, V>() : e)
            .toList() as V;
      } else {
        result[key as K] = this[key] as V;
      }
    }
    return result;
  }
}
