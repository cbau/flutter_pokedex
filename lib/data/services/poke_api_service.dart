import 'package:http/http.dart';

class PokeApiService {
  factory PokeApiService() => _instance;

  const PokeApiService._();

  static const _instance = PokeApiService._();

  String get serviceOrigin => 'https://pokeapi.co';

  Future<Response> getPokedexById(String id) =>
      get(Uri.parse('$serviceOrigin/api/v2/pokedex/$id'));

  Future<Response> getPokemonById(String id) =>
      get(Uri.parse('$serviceOrigin/api/v2/pokemon/$id'));
}
