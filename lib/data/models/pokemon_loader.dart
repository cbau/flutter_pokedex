// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'pokemon.dart';

class PokemonLoader {
  const PokemonLoader({
    this.items = const [],
    this.progress = 0,
  });

  final List<Pokemon> items;
  final double progress;
}
