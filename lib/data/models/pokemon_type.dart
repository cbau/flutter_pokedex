import 'pokemon_link.dart';

class PokemonType {
  const PokemonType({
    this.slot = 0,
    this.type = const PokemonLink(),
  });

  PokemonType.fromMap(Map<String, Object?>? map)
      : this(
          slot: map?['slot'] as int? ?? 0,
          type: PokemonLink.fromMap(map?['type'] as Map<String, Object?>?),
        );

  final int slot;
  final PokemonLink type;

  Map<String, Object?> toMap() => {
        'slot': slot,
        'type': type.toMap(),
      };
}
