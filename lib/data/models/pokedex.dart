import 'pokemon_entry.dart';

class Pokedex {
  const Pokedex({
    this.name = '',
    this.pokemonEntries = const [],
  });

  Pokedex.fromMap(Map<String, Object?>? map)
      : this(
          name: map?['name'] as String? ?? '',
          pokemonEntries: (map?['pokemon_entries'] as List?)
                  ?.map((x) => PokemonEntry.fromMap(x as Map<String, Object?>?))
                  .toList() ??
              [],
        );

  final String name;
  final List<PokemonEntry> pokemonEntries;

  Map<String, Object?> toMap() => {
        'name': name,
        'pokemon_entries': pokemonEntries.map((x) => x.toMap()).toList(),
      };
}
