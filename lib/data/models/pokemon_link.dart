class PokemonLink {
  const PokemonLink({
    this.name = '',
    this.url = '',
  });

  PokemonLink.fromMap(Map<String, Object?>? map)
      : this(
          name: map?['name'] as String? ?? '',
          url: map?['url'] as String? ?? '',
        );

  final String name;
  final String url;

  Map<String, Object?> toMap() => {
        'name': name,
        'url': url,
      };
}
