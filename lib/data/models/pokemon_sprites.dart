class PokemonSprites {
  const PokemonSprites({
    this.backDefault = '',
    this.backFemale = '',
    this.backShiny = '',
    this.backShinyFemale = '',
    this.frontDefault = '',
    this.frontFemale = '',
    this.frontShiny = '',
    this.frontShinyFemale = '',
  });

  PokemonSprites.fromMap(Map<String, Object?>? map)
      : this(
          backDefault: map?['back_default'] as String? ?? '',
          backFemale: map?['back_female'] as String? ?? '',
          backShiny: map?['back_shiny'] as String? ?? '',
          backShinyFemale: map?['back_shiny_female'] as String? ?? '',
          frontDefault: map?['front_default'] as String? ?? '',
          frontFemale: map?['front_female'] as String? ?? '',
          frontShiny: map?['front_shiny'] as String? ?? '',
          frontShinyFemale: map?['front_shiny_female'] as String? ?? '',
        );

  final String backDefault;
  final String backFemale;
  final String backShiny;
  final String backShinyFemale;
  final String frontDefault;
  final String frontFemale;
  final String frontShiny;
  final String frontShinyFemale;

  Map<String, Object?> toMap() => {
        'back_default': backDefault,
        'back_female': backFemale,
        'back_shiny': backShiny,
        'back_shiny_female': backShinyFemale,
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale,
      };
}
