import 'pokemon_sprites.dart';
import 'pokemon_type.dart';

class Pokemon {
  const Pokemon({
    this.description = '',
    this.height = 0,
    this.id = 0,
    this.name = '',
    this.sprites = const PokemonSprites(),
    this.types = const [],
    this.weight = 0,
  });

  Pokemon.fromMap(Map<String, Object?>? map)
      : this(
          height: map?['height'] as int? ?? 0,
          id: map?['id'] as int? ?? 0,
          name: map?['name'] as String? ?? '',
          sprites:
              PokemonSprites.fromMap(map?['sprites'] as Map<String, Object?>?),
          types: (map?['types'] as List?)
                  ?.map((x) => PokemonType.fromMap(x as Map<String, Object?>?))
                  .toList() ??
              [],
          weight: map?['weight'] as int? ?? 0,
        );

  final String description;
  final int height;
  final int id;
  final String name;
  final PokemonSprites sprites;
  final List<PokemonType> types;
  final int weight;

  Map<String, Object?> toMap() => {
        'height': height,
        'id': id,
        'name': name,
        'sprites': sprites.toMap(),
        'types': types.map((x) => x.toMap()).toList(),
        'weight': weight,
      };
}
