import 'pokemon_link.dart';

class PokemonEntry {
  const PokemonEntry({
    this.entryNumber = 0,
    this.pokemonSpecies = const PokemonLink(),
  });

  PokemonEntry.fromMap(Map<String, Object?>? map)
      : this(
          entryNumber: map?['entry_number'] as int? ?? 0,
          pokemonSpecies: PokemonLink.fromMap(
            map?['pokemon_species'] as Map<String, Object?>?,
          ),
        );

  final int entryNumber;
  final PokemonLink pokemonSpecies;

  Map<String, Object?> toMap() => {
        'entry_number': entryNumber,
        'pokemon_species': pokemonSpecies.toMap(),
      };
}
