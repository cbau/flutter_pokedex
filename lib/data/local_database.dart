import 'package:hive/hive.dart';

class LocalDatabase {
  factory LocalDatabase() => _instance;

  LocalDatabase._();

  static final _instance = LocalDatabase._();

  BoxCollection? _collection;

  CollectionBox<Map<dynamic, dynamic>?>? _pokemonBox;

  Future<BoxCollection> get collection async =>
      _collection ??= await _getCollection();

  Future<CollectionBox<Map<dynamic, dynamic>?>> get pokemonBox async =>
      _pokemonBox ??=
          await (await collection).openBox<Map<dynamic, dynamic>?>('pokemon');

  Future<BoxCollection> _getCollection() async => BoxCollection.open(
        'local',
        {
          'pokemon',
        },
      );
}
