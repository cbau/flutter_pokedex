import '../../data/models/pokemon.dart';

abstract class PokemonListState {
  const PokemonListState();
}

class PokemonListStateError extends PokemonListState {
  const PokemonListStateError({
    required this.error,
  });

  final String error;
}

class PokemonListStateLoading extends PokemonListState {
  const PokemonListStateLoading();
}

class PokemonListStateSuccess extends PokemonListState {
  const PokemonListStateSuccess({
    required this.items,
    required this.progress,
  });

  final List<Pokemon> items;
  final double progress;
}
