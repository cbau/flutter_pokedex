import 'package:flutter/material.dart';

import '../bloc/bloc_builder.dart';
import 'pokemon_list_bloc.dart';
import 'pokemon_list_item.dart';
import 'pokemon_list_state.dart';

class PokemonListPage extends StatelessWidget {
  const PokemonListPage({super.key});

  static const path = '/';

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Pokedex'),
        ),
        body: BlocBuilder(
          bloc: PokemonListBloc(),
          builder: (context, state) => Column(
            children: [
              if (state is PokemonListStateLoading ||
                  (state is PokemonListStateSuccess && state.progress < 1))
                LinearProgressIndicator(
                  value:
                      state is PokemonListStateSuccess ? state.progress : null,
                ),
              Expanded(
                child: state is PokemonListStateSuccess
                    ? GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithMaxCrossAxisExtent(
                          maxCrossAxisExtent: 480,
                          mainAxisExtent: 80,
                        ),
                        itemBuilder: (context, index) => PokemonListItem(
                          pokemon: state.items[index],
                        ),
                        itemCount: state.items.length,
                      )
                    : state is PokemonListStateError
                        ? Center(
                            child: Text(state.error),
                          )
                        : const Center(
                            child: Text(
                              'Loading…',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 12,
                              ),
                            ),
                          ),
              ),
            ],
          ),
        ),
      );
}
