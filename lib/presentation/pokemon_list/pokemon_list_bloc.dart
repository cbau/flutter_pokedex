import 'dart:async';

import 'package:flutter/material.dart';

import '../../data/repositories/pokemon_repository.dart';
import '../bloc/bloc.dart';
import 'pokemon_list_state.dart';

class PokemonListBloc extends Bloc<PokemonListState> {
  PokemonListBloc() : super(const PokemonListStateLoading()) {
    unawaited(_init());
  }

  Future<void> _init() async {
    PokemonRepository()
        .loadData()
        .listen(
          (event) => emit(
            PokemonListStateSuccess(
              items: event.items
                ..sort(
                  (a, b) => a.id == 0
                      ? b.id == 0
                          ? 0
                          : 1
                      : b.id == 0
                          ? -1
                          : a.id.compareTo(b.id),
                ),
              progress: event.progress,
            ),
          ),
        )
        // ignore: inference_failure_on_untyped_parameter
        .onError((e, s) {
      debugPrint('$e\n$s');
      emit(
        PokemonListStateError(
          error: e.toString(),
        ),
      );
    });
  }
}
