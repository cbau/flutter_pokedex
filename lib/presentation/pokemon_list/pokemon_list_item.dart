import 'package:flutter/material.dart';

import '../../data/models/pokemon.dart';
import '../pokemon_details/pokemon_details_page.dart';
import '../widgets/pokemon_type_chip.dart';

class PokemonListItem extends StatelessWidget {
  const PokemonListItem({
    required this.pokemon,
    super.key,
  });

  final Pokemon pokemon;

  @override
  Widget build(BuildContext context) => Card(
        child: InkWell(
          onTap: () async => pokemon.id > 0
              ? Navigator.of(context).pushNamed(
                  PokemonDetailsPage.path
                      .replaceFirst(':id', pokemon.id.toString()),
                  arguments: {
                    'pokemon': pokemon,
                  },
                )
              : null,
          child: Row(
            children: [
              AspectRatio(
                aspectRatio: 1,
                child: pokemon.sprites.frontDefault.isEmpty
                    ? const ColoredBox(color: Colors.grey)
                    : Hero(
                        tag: 'tag${pokemon.id}',
                        child: Image.network(pokemon.sprites.frontDefault),
                      ),
              ),
              Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(pokemon.name.toUpperCase()),
                    const SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: [
                        if (pokemon.types.isEmpty)
                          const PokemonTypeChip(type: '???')
                        else
                          ...pokemon.types.map(
                            (e) => PokemonTypeChip(type: e.type.name),
                          )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
}
