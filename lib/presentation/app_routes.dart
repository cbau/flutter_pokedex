import 'package:flutter/material.dart';

import '../data/models/pokemon.dart';
import 'pokemon_details/pokemon_details_bloc.dart';
import 'pokemon_details/pokemon_details_page.dart';
import 'pokemon_list/pokemon_list_page.dart';

class AppRoutes {
  factory AppRoutes() => _instance;

  const AppRoutes._();

  static const _instance = AppRoutes._();

  Map<String, Widget Function(BuildContext)> get routes => {
        PokemonListPage.path: (context) => const PokemonListPage(),
      };

  Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    if (settings.name?.startsWith('/pokemon/') ?? false) {
      final parts = settings.name!.split('/');
      final id = parts.length > 2 ? int.tryParse(parts[2]) ?? 0 : 0;
      final map = settings.arguments as Map<String, Object?>?;
      final pokemon = map?['pokemon'] as Pokemon?;
      return MaterialPageRoute(
        builder: (context) => PokemonDetailsPage(
          bloc: PokemonDetailsBloc(
            id: id,
            pokemon: pokemon,
          ),
        ),
        settings: settings,
      );
    }

    return null;
  }
}
