import 'package:flutter/material.dart';

class PokemonTypeChip extends StatelessWidget {
  const PokemonTypeChip({
    required this.type,
    super.key,
  });

  final String type;

  Map<String, Color> get _typeColors => const {
        'bug': Color(0xFFAABB22),
        'dark': Color(0xFF775544),
        'dragon': Color(0xFF7766EE),
        'electric': Color(0xFFFFCC33),
        'fairy': Color(0xFFEE99EE),
        'fighting': Color(0xFFBB5544),
        'fire': Color(0xFFFF4422),
        'flying': Color(0xFF8899FF),
        'ghost': Color(0xFF6666BB),
        'grass': Color(0xFF77CC55),
        'ground': Color(0xFFDDBB55),
        'ice': Color(0xFF66CCFF),
        'normal': Color(0xFFAAAA99),
        'poison': Color(0xFFAA5599),
        'psychic': Color(0xFFFF5599),
        'rock': Color(0xFFBBAA66),
        'steel': Color(0xFFAAAABB),
        'water': Color(0xFF3399FF),
      };

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 4,
        ),
        child: Chip(
          backgroundColor: _typeColors[type] ?? const Color(0xFFDBDBDB),
          label: Text(
            type.toUpperCase(),
            style: const TextStyle(
              color: Colors.white,
              fontSize: 10,
            ),
          ),
        ),
      );
}
