import 'package:flutter/material.dart';

import '../bloc/bloc_builder.dart';
import '../widgets/pokemon_type_chip.dart';
import 'pokemon_details_bloc.dart';
import 'pokemon_details_state.dart';

class PokemonDetailsPage extends StatelessWidget {
  const PokemonDetailsPage({
    required this.bloc,
    super.key,
  });

  static const path = '/pokemon/:id';

  final PokemonDetailsBloc bloc;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Pokedex'),
        ),
        body: BlocBuilder<PokemonDetailsBloc, PokemonDetailsState>(
          bloc: bloc,
          builder: (context, state) => state is PokemonDetailsStateSuccess
              ? ListView(
                  padding: const EdgeInsets.all(16),
                  children: [
                    SizedBox(
                      height: 96,
                      child: state.pokemon.sprites.frontDefault.isEmpty
                          ? const ColoredBox(color: Colors.grey)
                          : Hero(
                              tag: 'tag${state.pokemon.id}',
                              child: Image.network(
                                state.pokemon.sprites.frontDefault,
                              ),
                            ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(state.pokemon.name.toUpperCase()),
                    const SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: [
                        ...state.pokemon.types.map(
                          (e) => PokemonTypeChip(type: e.type.name),
                        )
                      ],
                    ),
                    Text(state.pokemon.description),
                  ],
                )
              : state is PokemonDetailsStateError
                  ? Center(
                      child: Text(state.error),
                    )
                  : const Center(
                      child: CircularProgressIndicator(),
                    ),
        ),
      );
}
