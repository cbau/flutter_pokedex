import 'dart:async';

import 'package:flutter/material.dart';

import '../../data/models/pokemon.dart';
import '../../data/models/pokemon_sprites.dart';
import '../../data/repositories/pokemon_repository.dart';
import '../bloc/bloc.dart';
import 'pokemon_details_state.dart';

class PokemonDetailsBloc extends Bloc<PokemonDetailsState> {
  PokemonDetailsBloc({
    required int id,
    Pokemon? pokemon,
  }) : super(
          pokemon == null
              ? const PokemonDetailsStateLoading()
              : PokemonDetailsStateSuccess(pokemon: pokemon),
        ) {
    if (pokemon == null) {
      unawaited(_loadPokemonById(id));
    }
  }

  Future<void> _loadPokemonById(int id) async {
    try {
      final pokemon = await PokemonRepository().getById(id);
      emit(
        PokemonDetailsStateSuccess(
          pokemon: pokemon,
        ),
      );
    } on Exception catch (e, s) {
      debugPrint('$e\n$s');
      emit(
        PokemonDetailsStateSuccess(
          pokemon: Pokemon(
            description: e.toString(),
            id: id,
            name: 'MissingNo.',
            sprites: const PokemonSprites(
              frontDefault:
                  'https://static.wikia.nocookie.net/espokemon/images/d/d8/MissingNo..png',
            ),
          ),
        ),
      );
    }
  }
}
