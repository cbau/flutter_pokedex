import '../../data/models/pokemon.dart';

abstract class PokemonDetailsState {
  const PokemonDetailsState();
}

class PokemonDetailsStateError extends PokemonDetailsState {
  const PokemonDetailsStateError({
    required this.error,
  });

  final String error;
}

class PokemonDetailsStateLoading extends PokemonDetailsState {
  const PokemonDetailsStateLoading();
}

class PokemonDetailsStateSuccess extends PokemonDetailsState {
  const PokemonDetailsStateSuccess({
    required this.pokemon,
  });

  final Pokemon pokemon;
}
