import 'package:flutter/material.dart';

class AppTheme {
  factory AppTheme() => _instance;

  const AppTheme._();

  static const _instance = AppTheme._();

  ThemeData get dark => _getThemeData(isDark: true);

  ThemeData get light => _getThemeData();

  ThemeData _getThemeData({bool isDark = false}) => ThemeData(
        brightness: isDark ? Brightness.dark : Brightness.light,
        colorSchemeSeed: Colors.red,
      );
}
