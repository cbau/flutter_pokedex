import 'dart:async';

abstract class Bloc<S> {
  Bloc(S state) : _state = state;

  final _controller = StreamController<S>.broadcast();
  S _state;

  S get state => _state;

  Stream<S> get stream => _controller.stream;

  Future<void> dispose() async => _controller.close();

  void emit(S state) {
    _state = state;
    _controller.add(state);
  }
}
