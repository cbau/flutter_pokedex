import 'dart:async';

import 'package:flutter/material.dart';

import 'bloc.dart';

class BlocBuilder<B extends Bloc<S>, S> extends StatefulWidget {
  const BlocBuilder({
    required this.bloc,
    required this.builder,
    super.key,
  });

  final B bloc;

  final Widget Function(BuildContext, S) builder;

  @override
  State<BlocBuilder<B, S>> createState() => _BlocBuilderState<B, S>();
}

class _BlocBuilderState<B extends Bloc<S>, S> extends State<BlocBuilder<B, S>> {
  @override
  Widget build(BuildContext context) => StreamBuilder<S>(
        builder: (context, snapshot) => snapshot.hasData
            ? widget.builder(context, snapshot.data as S)
            : const SizedBox(),
        initialData: widget.bloc.state,
        stream: widget.bloc.stream,
      );

  @override
  void dispose() {
    super.dispose();
    unawaited(widget.bloc.dispose());
  }
}
