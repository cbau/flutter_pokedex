import 'package:flutter/material.dart';

import 'app_routes.dart';
import 'app_theme.dart';
import 'pokemon_list/pokemon_list_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) => MaterialApp(
        darkTheme: AppTheme().dark,
        initialRoute: PokemonListPage.path,
        onGenerateRoute: AppRoutes().onGenerateRoute,
        routes: AppRoutes().routes,
        theme: AppTheme().light,
      );
}
