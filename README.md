# Flutter Pokedex

Pokedex made with Flutter, consuming data from the PokeAPI.

## Features

- The app consumes data from the [PokeAPI](https://pokeapi.co).
- Data is persisted locally with Hive.
- As data is initially consumed from multiple endpoints, data is served partially with a Stream to display data as it loads (See the function `loadData` at `PokemonRepository`).

## Demo

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/flutter_pokedex/)

## Screenshots

<img src="./screenshots/pokemon-list.png" alt="Pokemon list on small screens screenshot" width="240" /> <img src="./screenshots/pokemon-details.png" alt="Pokemon details on small screens screenshot" width="240" />

## Compile

To run this project on your device, run this command:

```bash
flutter run --release
```
